package com.example.consuapi.interfaces;

import com.example.consuapi.models.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PostService {

    @GET("post")
    Call<List<Post>> find(@Query("q") String q);
}
